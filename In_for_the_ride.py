FUEL_PRICE = 7.58
KM_PER_LITRE = 10

members_sum = int(input("Enter the number of members\n"))
driving_distance = float(input("Enter the driving distance\n"))

needed_money = driving_distance * FUEL_PRICE / KM_PER_LITRE
money_per_member = needed_money / members_sum

print("Money per member:", money_per_member)