import math

a = int(input("Enter a number of quadratic equation\n"))
b = int(input("Enter b number of quadratic equation\n"))
c = int(input("Enter c number of quadratic equation\n"))

first_result = (-b + math.sqrt((b*b) - (4*a*c))) / (2*a)

second_result = (-b - math.sqrt((b*b) - (4*a*c))) / (2*a)

print("The results are %d and %d" % (first_result, second_result))
