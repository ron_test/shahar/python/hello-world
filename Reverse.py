num = int(input("Enter 3 digits number\n"))

digit1 = num // 100
digit2 = (num // 10) % 10
digit3 = num % 10

reversed_num = digit3 * 100 + digit2 * 10 + digit1

print("Reversed number:", reversed_num)
